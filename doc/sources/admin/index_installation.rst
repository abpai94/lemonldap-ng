Installation
============

.. toctree::
   :maxdepth: 1

   prereq
   quickstart
   Download <https://lemonldap-ng.org/download.html>
   installdeb
   installrpm
   docker
